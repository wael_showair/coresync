//
//  AppDelegate.swift
//  Sync
//
//  Created by innovapost on 2017-11-27.
//  Copyright © 2017 innovapost. All rights reserved.
//

import UIKit
import iCloudStore
import CoreDataStore
import CoreSync

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let viewController = mainStoryboard.instantiateInitialViewController() as! ViewController
    viewController.localeStore = CoreDataStore(name: "Person")
    viewController.iCloud = ICloudStore()
    viewController.pipeline = Pipeline()
    window?.rootViewController = viewController
    return true
  }
}

